module gitlab.com/go-cmds/goarp

go 1.20

require (
 gitlab.com/go-cmds/goarp/goarpports latest
 gitlab.com/go-cmds/goarp/goarpscan  latest
 gitlab.com/go-cmds/goarp/goarpping  latest
)
